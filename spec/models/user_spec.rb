require 'rails_helper'

RSpec.describe User, type: :model do
  it "is valid with a name, and email" do 
    user = User.new(
      name: "Michael",
      email: "test@example.com"
    )
    expect(user).to be_valid
  end

  it "is invalid without a name" do
    user = User.new(name: nil)
    user.valid?
    expect(user.errors[:name]).to include("can't be blank")
  end

  it "is invalid without a email" do
    user = User.new(email: nil)
    expect(user).to_not be_valid
  end

  it "is invalid with a duplicate email"
  it "is returns a user's full name as a string"
end
