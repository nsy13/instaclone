# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!(name: "Shuhei Yoshino",
            email: "s.yoshino.1113@gmail.com",
            password: "hogehoge",
            password_confirmation: "hogehoge",
            admin: true,
            activated: true,
            activated_at: Time.zone.now)

99.times do |n|
  name = Faker::Games::Pokemon.name
  email = Faker::Internet.email
  password = "hogehoge"
  password_confirmation = "hogehoge"
  User.create!(name: name,
              email: email,
              password: password,
              password_confirmation: password_confirmation,
              activated: true,
              activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(content: content) }
end