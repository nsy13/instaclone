class RemoveUsersIdFromPost < ActiveRecord::Migration[5.2]
  def change
    remove_column :posts, :users_id, :integer
  end
end
