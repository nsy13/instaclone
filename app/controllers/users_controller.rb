class UsersController < ApplicationController
  # ログインしてなくてもuser indexやuser showは見れるようにする
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]
  # pagenationの上限を規定
  PER = 30

  def new
    @user = User.new
  end

  def create
<<<<<<< HEAD
    @user = User.new(user_params)
    if @user.save
      UserMailer.account_activation(@user).deliver_now
      flash[:info] = "メールアドレス確認のメールを送信しました。リンクからアカウントを有効化してください"
      # login @user
      # flash[:info] = "ユーザー登録しました"
      redirect_to root_path
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
    @posts = @user.posts.page(params[:page]).per(PER)
  end

  def index
    @users = User.page(params[:page]).per(PER)
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "プロフィールを変更しました"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    user = User.find(params[:id])
    user.delete
    flash[:success] = "退会しました"
    redirect_to root_path
  end

  private
    def user_params
      params.require(:user).permit(:name, :email, :password, :password_confirmation)
    end

    def logged_in_user
      unless logged_in?
        flash[:danger] = "ログインしてください"
        store_lacation
        redirect_to login_path
      end
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to root_path unless correct_user?(@user)
    end
end
