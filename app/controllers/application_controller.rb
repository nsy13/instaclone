class ApplicationController < ActionController::Base
  include CommonActions
  # include SessionsHelper

  private
    def logged_in_user
      unless logged_in?
        flash[:danger] = "ログインしてください"
        redirect_to login_path
      end
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "ログインしてください"
        redirect_to login_path
      end
    end
    # users_controller内のlogged_in_userは削除する

end
