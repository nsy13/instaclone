class StaticPagesController < ApplicationController
  PER = 30

  def top
    @post = current_user.posts.build if logged_in?
    @feed_items = current_user.feed.page(params[:page]).per(PER)
  end
end
