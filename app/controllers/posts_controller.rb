class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destory]
  before_action :correct_user, only: [:destroy]
  PER = 30

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "投稿しました"
      redirect_to root_path
    else
      @feed_items = []
      render "static_pages/top"
    end
  end

  def destroy
    @post.destroy
    flash[:success] = "ポストを削除しました"
    redirect_to request.referrer || root_path
  end

  def index
    @posts = Post.page(params[:page]).per(PER)
  end
end

  private
    def post_params
      params.require(:post).permit(:content, :picture)
    end

    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_path if @post.nil?
    end