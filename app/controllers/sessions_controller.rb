class SessionsController < ApplicationController
  def new
  end

  def create
    # userをdbから探し出す
    user = User.find_by(email: params[:session][:email].downcase)
    # userが存在し、かつ、パスワード認証が通るか確認
    if user && user.authenticate(params[:session][:password])
      if user.activated?
        # user.idをセッションの中のuser_idに一時保存
        login user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user
      else
        message = "アカウントが有効化されていません"
        message += "メールを確認してください"
        flash[:warning] = message
        redirect_to root_path
      end
    else
      flash.now[:danger] = "メールアドレス、またはパスワードに誤りがあります"
      render 'new'
    end
  end

  def destroy
    logout if logged_in?
    redirect_to root_path
  end
end
