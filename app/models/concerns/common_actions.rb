require 'active_support/concern'

module CommonActions
  extend ActiveSupport::Concern

  included do
    helper_method :current_user, :logged_in?, :correct_user?
  end

  def login(user)
    session[:user_id] = user.id
  end

  def current_user
    # sessionに保存したuser_idを変数user_idに代入できる場合
    # 比較演算子ではなく、代入！
    if user_id = session[:user_id]
      # すでに@current_userが存在している場合には省エネしたい
      @current_user ||= User.find_by(id: session[:user_id])
    elsif user_id = cookies.signed[:user_id]
      user = User.find_by(id: user_id)
      if user && authenticated?(:remember, cookies.signed[remember_token])
        login user
        @current_user = user
      end
    end
  end

  def correct_user?(user)
    user == current_user
  end

  def logged_in?
    !current_user.nil?
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def logout
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent.signed[:remember_token] = user.remember_token
  end

  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
end
